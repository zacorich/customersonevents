package co.uk.juicify.customersOnEvents.server.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name="EVENTS")
public class Event implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EVENT_ID")
    private Long id;

    @Column(name = "EVENT_NAME")
    private String name;

    @Column(name = "EVENT_PLACE")
    private String place;

    @Column(name = "EVENT_START")
    private Timestamp start;

    @Column(name = "EVENT_FINISH")
    private Timestamp finish;

    @Column(name = "EVENT_CREATED")
    private Timestamp created;

    @ManyToMany
    @JoinTable(name = "EVENT_CUSTOMERS", joinColumns = { @JoinColumn(name = "CUSTOMER_ID", nullable = true) }, inverseJoinColumns = { @JoinColumn(name = "EVENT_ID") })
    private Set<Customer> customers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getFinish() {
        return finish;
    }

    public void setFinish(Timestamp finish) {
        this.finish = finish;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Set<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(Set<Customer> customers) {
        this.customers = customers;
    }
}
