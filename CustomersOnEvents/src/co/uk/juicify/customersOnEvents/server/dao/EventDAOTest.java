package co.uk.juicify.customersOnEvents.server.dao;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Timestamp;
import java.util.Calendar;

import co.uk.juicify.customersOnEvents.server.pojo.Event;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EventDAOTest {

	EventDAO eventDAO = new EventDAO();
	Event event1;
    Event event2;
	Long savedEvent1id;
	Long savedEvent2id;

	@Before
	public void setUp() throws Exception {
		event1 = new Event();
		event1.setName("Jim's Birthday party");
		event1.setCreated(new Timestamp(Calendar.getInstance().getTimeInMillis()));//current time
        event1.setStart(new Timestamp(Calendar.getInstance().getTimeInMillis()+10000));//10 seconds to future
        event1.setFinish(new Timestamp(Calendar.getInstance().getTimeInMillis() + 20000));//20 seconds to future

        event2 = new Event();
        event2.setName("Tanja's Birthday party");
        event2.setCreated(new Timestamp(Calendar.getInstance().getTimeInMillis()));//current time
        event2.setStart(new Timestamp(Calendar.getInstance().getTimeInMillis()+100000));//100 seconds to future
        event2.setFinish(new Timestamp(Calendar.getInstance().getTimeInMillis() + 200000));//200 seconds to future

    }
	
	@Test
	public void saveGetDelete() {
		savedEvent1id = eventDAO.save(event1);
		savedEvent2id = eventDAO.save(event2);

        //ids must exist
        assertNotNull(savedEvent1id);
        assertNotNull(savedEvent2id);

        //ids created must equal actual ids
		assertEquals(savedEvent1id, eventDAO.getById(savedEvent1id).getId());
		assertEquals(savedEvent2id, eventDAO.getById(savedEvent2id).getId());

		eventDAO.deleteById(savedEvent1id);
		eventDAO.deleteById(savedEvent2id);
		
	}


	
	@After
	public void tearDown() throws Exception {
	}

}
