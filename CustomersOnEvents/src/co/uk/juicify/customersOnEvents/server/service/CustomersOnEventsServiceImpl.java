package co.uk.juicify.customersOnEvents.server.service;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import co.uk.juicify.customersOnEvents.client.CustomersOnEventsService;

public class CustomersOnEventsServiceImpl extends RemoteServiceServlet implements CustomersOnEventsService {
    // Implementation of sample interface method
    public String getMessage(String msg) {
        return "Client said: \"" + msg + "\"<br>Server answered: \"Hi!\"";
    }
}