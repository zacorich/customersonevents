package co.uk.juicify.customersOnEvents.client;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;

/**
 * Created with IntelliJ IDEA.
 * User: constantin
 * Date: 08/05/2012
 * Time: 02:29
 * Registration Form
 */
public class Registration extends Composite {
    private TextBox textBoxEmail;
    private TextBox textBoxSurname;
    private TextBox textBoxName;
    private DateBox dateBoxBirthday;
    private PasswordTextBox passwordTextBoxPass2;
    private PasswordTextBox passwordTextBoxPass1;

    public Registration() {

        VerticalPanel verticalPanel = new VerticalPanel();
        initWidget(verticalPanel);
        verticalPanel.setSize("332px", "257px");

        Label lblRegisterNewCustomer = new Label("New Customer Registration");
        verticalPanel.add(lblRegisterNewCustomer);

        FlexTable flexTable = new FlexTable();
        verticalPanel.add(flexTable);

        Label lblEmail = new Label("E-mail");
        flexTable.setWidget(0, 0, lblEmail);

        textBoxEmail = new TextBox();
        flexTable.setWidget(0, 1, textBoxEmail);

        Label lblName = new Label("Name");
        flexTable.setWidget(1, 0, lblName);

        textBoxName = new TextBox();
        flexTable.setWidget(1, 1, textBoxName);

        Label lblSurname = new Label("Surname");
        flexTable.setWidget(2, 0, lblSurname);

        textBoxSurname = new TextBox();
        flexTable.setWidget(2, 1, textBoxSurname);

        Label lblPassword = new Label("Password");
        flexTable.setWidget(3, 0, lblPassword);

        passwordTextBoxPass1 = new PasswordTextBox();
        flexTable.setWidget(3, 1, passwordTextBoxPass1);

        Label lblRepeatPassword = new Label("Repeat Password");
        flexTable.setWidget(4, 0, lblRepeatPassword);

        passwordTextBoxPass2 = new PasswordTextBox();
        flexTable.setWidget(4, 1, passwordTextBoxPass2);

        Label lblBirthday = new Label("Birthday");
        flexTable.setWidget(5, 0, lblBirthday);

        dateBoxBirthday = new DateBox();
        flexTable.setWidget(5, 1, dateBoxBirthday);

        Button btnRegister = new Button("Register!");
        btnRegister.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if(passwordTextBoxPass1.getText().compareTo(passwordTextBoxPass2.getText())!=0){
                    Window.alert("Password you entered doesn't match!");
                }
                if(passwordTextBoxPass1.getText().isEmpty() || passwordTextBoxPass2.getText().isEmpty()){
                    Window.alert("Password cannot be empty!");
                }
            }
        });
        flexTable.setWidget(6, 1, btnRegister);
        flexTable.getCellFormatter().setVerticalAlignment(6, 1, HasVerticalAlignment.ALIGN_MIDDLE);
        flexTable.getCellFormatter().setHorizontalAlignment(6, 1, HasHorizontalAlignment.ALIGN_RIGHT);
    }
}
