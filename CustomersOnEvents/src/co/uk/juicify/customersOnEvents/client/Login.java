package co.uk.juicify.customersOnEvents.client;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;

/**
 * Created with IntelliJ IDEA.
 * User: constantin
 * Date: 08/05/2012
 * Time: 02:19
 * Login Form
 */
public class Login extends Composite{
    private TextBox textBoxUsername;
    private TextBox textBoxPassword;

    public Login() {

        VerticalPanel verticalPanel = new VerticalPanel();
        initWidget(verticalPanel);
        verticalPanel.setSize("301px", "189px");

        Label lblLoginToYour = new Label("Login into your account");
        lblLoginToYour.setStyleName("gwt-Label-Login");
        verticalPanel.add(lblLoginToYour);

        FlexTable flexTable = new FlexTable();
        verticalPanel.add(flexTable);
        flexTable.setWidth("293px");

        Label lblUsername = new Label("Username:");
        lblUsername.setStyleName("gwt-Label-Login");
        flexTable.setWidget(0, 0, lblUsername);
        lblUsername.setSize("66px", "27px");

        textBoxUsername = new TextBox();
        flexTable.setWidget(0, 1, textBoxUsername);

        Label lblPassword = new Label("Password:");
        lblPassword.setStyleName("gwt-Label-Login");
        flexTable.setWidget(1, 0, lblPassword);

        textBoxPassword = new TextBox();
        flexTable.setWidget(1, 1, textBoxPassword);

        CheckBox chckbxRememberMe = new CheckBox("Remember me");
        chckbxRememberMe.setStyleName("gwt-CheckBox-New");
        flexTable.setWidget(2, 1, chckbxRememberMe);

        Button btnLogin = new Button("Login");
        btnLogin.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if(textBoxUsername.getText().length() == 0 || textBoxPassword.getText().length() == 0){
                    Window.alert("Username and Password cannot be blank!");
                }
            }
        });
        flexTable.setWidget(3, 1, btnLogin);
    }

}
