package co.uk.juicify.customersOnEvents.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface CustomersOnEventsServiceAsync {
    void getMessage(String msg, AsyncCallback<String> async);
}
