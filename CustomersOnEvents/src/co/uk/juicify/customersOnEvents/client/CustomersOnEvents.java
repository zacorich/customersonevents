package co.uk.juicify.customersOnEvents.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.DOM;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;

/**
 * Entry point classes define <code>onModuleLoad()</code>
 */
public class CustomersOnEvents implements EntryPoint {

    /**
     * This is the entry point method.
     */
    public static DeckPanel deckPanel = new DeckPanel();

    public void onModuleLoad() {
        RootPanel rootPanel = RootPanel.get();

        HorizontalPanel horizontalPanel = new HorizontalPanel();

        horizontalPanel.add(deckPanel);
        deckPanel.setAnimationEnabled(true);

        VerticalPanel verticalPanel = new VerticalPanel();
        deckPanel.add(verticalPanel);

        Login login = new Login();
        verticalPanel.add(login);

        Button btnNewCustomer = new Button("New Customer?");
        btnNewCustomer.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                //show registration form
                CustomersOnEvents.deckPanel.showWidget(1);
            }
        });
        verticalPanel.add(btnNewCustomer);

        VerticalPanel verticalPanel_1 = new VerticalPanel();
        deckPanel.add(verticalPanel_1);

        Registration registration = new Registration();
        verticalPanel_1.add(registration);

        Button btnExistingCustomer = new Button("Existing Customer?");
        btnExistingCustomer.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                //show login form if already resitered
                CustomersOnEvents.deckPanel.showWidget(0);
            }
        });
        verticalPanel_1.add(btnExistingCustomer);
        deckPanel.showWidget(0);

        rootPanel.add(horizontalPanel);
    }

    private static class MyAsyncCallback implements AsyncCallback<String> {
        private Label label;

        public MyAsyncCallback(Label label) {
            this.label = label;
        }

        public void onSuccess(String result) {
            label.getElement().setInnerHTML(result);
        }

        public void onFailure(Throwable throwable) {
            label.setText("Failed to receive answer from server!");
        }
    }
}
