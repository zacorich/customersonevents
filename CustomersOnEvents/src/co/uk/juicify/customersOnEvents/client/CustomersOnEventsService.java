package co.uk.juicify.customersOnEvents.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("CustomersOnEventsService")
public interface CustomersOnEventsService extends RemoteService {
    // Sample interface method of remote interface
    String getMessage(String msg);

    /**
     * Utility/Convenience class.
     * Use CustomersOnEventsService.App.getInstance() to access static instance of CustomersOnEventsServiceAsync
     */
    public static class App {
        private static CustomersOnEventsServiceAsync ourInstance = GWT.create(CustomersOnEventsService.class);

        public static synchronized CustomersOnEventsServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
